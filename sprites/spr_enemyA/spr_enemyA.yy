{
    "id": "8d9ac7fd-8263-4013-9c46-26c91c194cef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemyA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 9,
    "bbox_right": 38,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3df91812-02cd-4a5f-8bf0-aa1c56f0977e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d9ac7fd-8263-4013-9c46-26c91c194cef",
            "compositeImage": {
                "id": "6d50805a-b250-4d9d-b70c-a06adca86abc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3df91812-02cd-4a5f-8bf0-aa1c56f0977e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dd8b435-0d26-4cd9-9077-684d40f48048",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3df91812-02cd-4a5f-8bf0-aa1c56f0977e",
                    "LayerId": "5f22ab74-05e0-474f-adf9-17cac6926446"
                }
            ]
        },
        {
            "id": "c99f5c6c-e630-40da-a8a1-65475e0a3b72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d9ac7fd-8263-4013-9c46-26c91c194cef",
            "compositeImage": {
                "id": "146ea288-1b9c-4a97-80a2-83426d096d11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c99f5c6c-e630-40da-a8a1-65475e0a3b72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ba5c5db-af77-4ccd-909f-c76870e68f88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c99f5c6c-e630-40da-a8a1-65475e0a3b72",
                    "LayerId": "5f22ab74-05e0-474f-adf9-17cac6926446"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "5f22ab74-05e0-474f-adf9-17cac6926446",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d9ac7fd-8263-4013-9c46-26c91c194cef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}