{
    "id": "31940200-4732-4ea5-9200-015dcc4ff65f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_playerR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 18,
    "bbox_right": 30,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b33dbd3-5c69-43f8-be39-3ef10605afa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31940200-4732-4ea5-9200-015dcc4ff65f",
            "compositeImage": {
                "id": "fc80c198-1c63-4399-b5ca-b58cb2aa417c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b33dbd3-5c69-43f8-be39-3ef10605afa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b547d4c8-def0-4708-8c1a-822722623eaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b33dbd3-5c69-43f8-be39-3ef10605afa5",
                    "LayerId": "c0e2e9d7-b80a-4d36-9e17-8f9f72bbe920"
                }
            ]
        },
        {
            "id": "8983a6ea-7772-4885-b898-be001537736c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31940200-4732-4ea5-9200-015dcc4ff65f",
            "compositeImage": {
                "id": "2c343bcf-9a11-4046-9077-758527361a9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8983a6ea-7772-4885-b898-be001537736c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f603437d-be43-4e8f-b02f-370d6cefadad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8983a6ea-7772-4885-b898-be001537736c",
                    "LayerId": "c0e2e9d7-b80a-4d36-9e17-8f9f72bbe920"
                }
            ]
        },
        {
            "id": "ee99dbe2-9e74-40f5-a5b7-20b71d9a62fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31940200-4732-4ea5-9200-015dcc4ff65f",
            "compositeImage": {
                "id": "7f3b7702-c138-4d60-9a81-00b69630ad97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee99dbe2-9e74-40f5-a5b7-20b71d9a62fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35e3b45c-b376-4f2a-82f7-842f3a8bc880",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee99dbe2-9e74-40f5-a5b7-20b71d9a62fe",
                    "LayerId": "c0e2e9d7-b80a-4d36-9e17-8f9f72bbe920"
                }
            ]
        },
        {
            "id": "ada17c3f-777d-4935-a3e0-5288ff6dadbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31940200-4732-4ea5-9200-015dcc4ff65f",
            "compositeImage": {
                "id": "51e20273-cd1c-4286-b4c1-8b3564b48754",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ada17c3f-777d-4935-a3e0-5288ff6dadbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbd9d5c0-3868-4111-81f8-f9307de660d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ada17c3f-777d-4935-a3e0-5288ff6dadbf",
                    "LayerId": "c0e2e9d7-b80a-4d36-9e17-8f9f72bbe920"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "c0e2e9d7-b80a-4d36-9e17-8f9f72bbe920",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31940200-4732-4ea5-9200-015dcc4ff65f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}