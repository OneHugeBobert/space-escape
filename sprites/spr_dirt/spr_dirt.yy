{
    "id": "f0edc29e-126c-4c9a-9cb9-d9af0b815f66",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dirt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "935bd4ed-8baf-4874-bfe2-82d490ace5ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0edc29e-126c-4c9a-9cb9-d9af0b815f66",
            "compositeImage": {
                "id": "34ac90f2-27fa-4104-9db5-8eb6bdc56f1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "935bd4ed-8baf-4874-bfe2-82d490ace5ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8c0ca5b-28f3-496f-a6c4-dc7c3bbab4e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "935bd4ed-8baf-4874-bfe2-82d490ace5ed",
                    "LayerId": "4766ef0c-1e8e-4b0e-ad80-78b11a4503d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "4766ef0c-1e8e-4b0e-ad80-78b11a4503d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f0edc29e-126c-4c9a-9cb9-d9af0b815f66",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 48
}