{
    "id": "21fda5f5-c0c8-4b9c-b079-cf62243db06b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_planets",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 886,
    "bbox_left": 84,
    "bbox_right": 1892,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b51d126-4d4e-446a-937d-006d4eac20bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21fda5f5-c0c8-4b9c-b079-cf62243db06b",
            "compositeImage": {
                "id": "4dd0272e-3ca0-4338-ad69-bf193d669a38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b51d126-4d4e-446a-937d-006d4eac20bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fad7ad15-09bb-4a70-ad05-ab8bf446378b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b51d126-4d4e-446a-937d-006d4eac20bf",
                    "LayerId": "5b53ed07-a17d-44ba-b56d-4c38f13703a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "5b53ed07-a17d-44ba-b56d-4c38f13703a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21fda5f5-c0c8-4b9c-b079-cf62243db06b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2048,
    "xorig": 1024,
    "yorig": 512
}