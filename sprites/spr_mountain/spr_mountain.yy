{
    "id": "d0a9a70f-304d-4f12-b90d-386d19922cb2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mountain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 110,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8162bd8a-f378-4211-9dfe-3c2aada1d304",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0a9a70f-304d-4f12-b90d-386d19922cb2",
            "compositeImage": {
                "id": "68fecdfa-05d3-4c72-a6cc-2bd47556ea30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8162bd8a-f378-4211-9dfe-3c2aada1d304",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b67d223-e92f-4a06-a6d2-2f488231f4f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8162bd8a-f378-4211-9dfe-3c2aada1d304",
                    "LayerId": "e51daa25-eecb-480d-8907-96c2793a822e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "e51daa25-eecb-480d-8907-96c2793a822e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d0a9a70f-304d-4f12-b90d-386d19922cb2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": true,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 240
}