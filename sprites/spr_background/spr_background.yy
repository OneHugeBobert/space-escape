{
    "id": "42ae804c-84f9-4237-97ea-6bc150bafbcb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1023,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28ef9362-5016-4dfb-be56-6d080f81c86d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42ae804c-84f9-4237-97ea-6bc150bafbcb",
            "compositeImage": {
                "id": "656ac883-76a0-43dc-bcc1-380a1218821c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28ef9362-5016-4dfb-be56-6d080f81c86d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "503c57c5-9136-4d8f-b1c6-95425c174ef9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28ef9362-5016-4dfb-be56-6d080f81c86d",
                    "LayerId": "5c6ce483-9d37-4064-a7ce-a38f7c172a0f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "5c6ce483-9d37-4064-a7ce-a38f7c172a0f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "42ae804c-84f9-4237-97ea-6bc150bafbcb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}