{
    "id": "52b2e294-e768-4cdd-8b9e-a77bc8a712d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 9,
    "bbox_right": 38,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6b11ac7-1806-4d22-b8ed-985e9e653cff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52b2e294-e768-4cdd-8b9e-a77bc8a712d0",
            "compositeImage": {
                "id": "74a56e46-3709-4727-9bf2-61c73883497c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6b11ac7-1806-4d22-b8ed-985e9e653cff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa35820a-fadb-428d-b80d-21b3226dcf18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6b11ac7-1806-4d22-b8ed-985e9e653cff",
                    "LayerId": "81688536-cab1-46ca-b3ee-106e3f52eaef"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 48,
    "layers": [
        {
            "id": "81688536-cab1-46ca-b3ee-106e3f52eaef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52b2e294-e768-4cdd-8b9e-a77bc8a712d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}