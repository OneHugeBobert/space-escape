{
    "id": "d29763be-471d-47ae-86b9-c297beaba78d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemyD",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 15,
    "bbox_right": 32,
    "bbox_top": 35,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "36b99a21-7a93-4464-8682-7c03214df673",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d29763be-471d-47ae-86b9-c297beaba78d",
            "compositeImage": {
                "id": "2f933ae8-0065-4621-8f33-7afdbb761e77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36b99a21-7a93-4464-8682-7c03214df673",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0312b2f8-2b0a-40a5-bcb8-2ac6b440390e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36b99a21-7a93-4464-8682-7c03214df673",
                    "LayerId": "b9f7f792-4189-4686-8443-a1684c57f704"
                }
            ]
        },
        {
            "id": "da874565-b20d-4a29-b66a-b925d5abc8ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d29763be-471d-47ae-86b9-c297beaba78d",
            "compositeImage": {
                "id": "1d933f7f-1e1b-4a8f-9403-42ea5725a33b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da874565-b20d-4a29-b66a-b925d5abc8ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81390e9f-dfe8-472b-bdca-a62d39d54e71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da874565-b20d-4a29-b66a-b925d5abc8ae",
                    "LayerId": "b9f7f792-4189-4686-8443-a1684c57f704"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "b9f7f792-4189-4686-8443-a1684c57f704",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d29763be-471d-47ae-86b9-c297beaba78d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 21,
    "yorig": 40
}