{
    "id": "4b2700b5-4b0c-480b-b65c-0b80fa847629",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rocket",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 0,
    "bbox_right": 112,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b9d84cf5-200a-4c13-b9ec-30613cc3ba17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b2700b5-4b0c-480b-b65c-0b80fa847629",
            "compositeImage": {
                "id": "43e75c32-6e2b-471c-9b7f-1f7b44954fbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9d84cf5-200a-4c13-b9ec-30613cc3ba17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "477c1085-fa9b-4845-b688-40aff5edb55d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9d84cf5-200a-4c13-b9ec-30613cc3ba17",
                    "LayerId": "c7546ddc-0ac7-45b7-b134-dfb7bf5faaea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "c7546ddc-0ac7-45b7-b134-dfb7bf5faaea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b2700b5-4b0c-480b-b65c-0b80fa847629",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 113,
    "xorig": 0,
    "yorig": 0
}