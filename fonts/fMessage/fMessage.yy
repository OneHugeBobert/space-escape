{
    "id": "9d5c8c12-ea29-4a41-8a5d-190f51d6a400",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fMessage",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Droid Sans Mono",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e196c14c-22ce-42c6-b587-ade387c3d0c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "f3aadfbb-8ae4-4dcd-969d-082d8258c86f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 173,
                "y": 48
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "1b5c702d-981c-443a-bdf8-8894d835edbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 165,
                "y": 48
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "35ee11bb-fa33-450d-9a7c-541ebfba5058",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 153,
                "y": 48
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "03da3d5a-9ef7-43ae-a272-af87ba5a0e41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 143,
                "y": 48
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "7fb90702-5524-4122-a1bf-bd1a57d8e161",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 131,
                "y": 48
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "8d356306-9b33-4fbd-b2df-3d43b6923fdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 119,
                "y": 48
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "fe1ac717-7653-4779-bb68-e9adc9cd81be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 114,
                "y": 48
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c26c61d5-944c-4ec8-b158-da5248ecbda6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 106,
                "y": 48
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "75a096d4-1d82-4309-b379-97550a9faa14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 98,
                "y": 48
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "ba11a797-92b1-4caf-9ec5-62b3c043244f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 178,
                "y": 48
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "6a93d19b-6d89-4315-89ed-731e2fd83254",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 88,
                "y": 48
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "b2d5cc1c-a353-4b3d-94a6-21026cdbc622",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 72,
                "y": 48
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "5a91d0d6-0989-4c04-ba9f-2ca9112b7354",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 64,
                "y": 48
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "77e99907-a976-4512-a2d5-e1e8fe357afe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 59,
                "y": 48
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "eb57d20f-27d7-449e-acbe-117c37ef6844",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 50,
                "y": 48
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "77c5573a-153c-4559-89cb-1fad1845fd23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 40,
                "y": 48
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "29f61717-aa7b-4a94-9382-e0f7b4b25b10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 5,
                "x": 33,
                "y": 48
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "a386e4b2-1c62-464c-9745-cb77a3aa2619",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 23,
                "y": 48
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "5128f71c-6b77-4800-b411-1d06cdbf9cc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 13,
                "y": 48
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "45a8698c-56fd-4292-89bd-977648503fa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "01da46bc-ec93-45f5-8240-4241ed3be58a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 78,
                "y": 48
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "76e05bf3-096f-495e-80ed-de0143dcd89e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 188,
                "y": 48
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "a96112ce-226c-4e71-b0c9-ef49d8bbaacb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 198,
                "y": 48
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "28d124be-0137-4725-80bd-b0c849a26cf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 208,
                "y": 48
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "67c2b2ca-2d7e-4954-8b24-0074dfab89dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 166,
                "y": 71
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "1fa391e0-b4b6-4752-9d89-442c851b4c2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 161,
                "y": 71
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "cd7f8352-3465-48a9-a259-a60418b2696d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 156,
                "y": 71
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "aa08b054-e7d5-462f-adf9-6b9c4e4215d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 146,
                "y": 71
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b7808908-7c29-486d-ac69-c0f6a1acf40a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 136,
                "y": 71
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "32a582f0-06c0-45a7-8ce2-c1fa07972461",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 126,
                "y": 71
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "24555214-a8b7-4b41-8eff-605507b6db6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 116,
                "y": 71
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "de6fed82-5e94-4675-a16a-91c300cd5ad4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 104,
                "y": 71
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "94570525-1290-4ff3-912a-13912d918f12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 92,
                "y": 71
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "bf01580a-e319-48c5-bc9c-1aaef9c3c433",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 82,
                "y": 71
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "fe526fa9-e342-4956-848e-4b47ff0122ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 71,
                "y": 71
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "dd420ac8-9f66-4fe7-ab38-078852a3df5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 61,
                "y": 71
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "9a3404b3-5a94-4060-b7c4-3729e43ea4fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 51,
                "y": 71
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "34516cd0-29b8-464c-8d8c-64517cba81f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 41,
                "y": 71
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "aeb0c461-20f3-4435-8bc3-440c2c55944b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 30,
                "y": 71
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "145c4a19-c681-4227-925e-a7d971f58d1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 20,
                "y": 71
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "74d47b47-6e16-4854-95d4-d566f5396410",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 11,
                "y": 71
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e36bef4b-3a01-4806-90eb-1a554340b3f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 2,
                "y": 71
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "e3748d13-185b-41e8-93a5-e252cfdb5a47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 239,
                "y": 48
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "2307562b-e443-4232-b73d-4b4ce18fb660",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 229,
                "y": 48
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "6b7464d1-be2a-4184-b22e-8598e5822b8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 218,
                "y": 48
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "be12feba-1ece-412c-96b8-54e22d549923",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 241,
                "y": 25
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "c856b090-5e49-47de-92a2-46681d3f7de1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 230,
                "y": 25
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "27132812-24f0-4aec-9760-05fdc2c96ffb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 220,
                "y": 25
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "422c2423-f38d-436b-a0c8-02990da0ca69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "79e5641d-8931-41d9-a9cc-6ff8bef5be22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "f87a3cb9-3002-4376-b8aa-ee13646259f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "9e8d8e81-078f-49b4-a135-bfd57bd2e199",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "767050d2-240d-4c0c-9700-75544314e906",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 179,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "c01303ba-efd4-4653-a70a-eb70ac8f929b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "67dc2fe0-b828-44f6-b432-86f1488ddae0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ea72fd90-feaf-42d7-8398-a73e39cc1d51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "09039fcd-a80e-43f6-adc7-5c55b234bfc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 131,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "9aa33e5b-8dcc-4235-9416-a9f705128828",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "7042299a-359a-41cf-9edc-937ad9757376",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "88b6567d-24f8-4153-b588-5bac45b88ec6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "032242b2-3eb6-4012-b4c3-8e3faaffe7a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "30ee4c5d-896d-4b0e-84d3-49387cecb5f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e5d15ae3-77b2-429e-9fc9-39c5b13ece57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "0da6b6e4-2980-441d-aaaa-dceacaa1d5c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "7eba0ae2-59d1-402f-9ca8-6c58e70be056",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "3cff25c3-6475-4e88-bf85-33973ceed414",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5d0b7c8e-7ba1-4a8a-9261-816d65ae12c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "74176a22-0cf5-42bf-9c2a-ae52b9febc47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "73cf33ca-9320-4ac3-969e-d9d47ba4f786",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "c91cf626-e856-4bfc-97d0-4922e0895bb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "4e55858c-9efb-43ca-92da-70b1392c3ccb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d5bfda50-2df7-4128-bede-8b520f162568",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 100,
                "y": 25
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "2b6869b5-820e-45ce-8a12-12c898f2287b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "3bb7be5b-66c9-4a49-ac77-1e08ae956c3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 202,
                "y": 25
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "dc6ab07f-84e9-4934-81cc-47d53d64d3dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 191,
                "y": 25
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "27fe9abd-ec45-4f1e-9596-aa0ede0f22af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 181,
                "y": 25
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "e168540b-3179-4790-8a7d-e2ed30454235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 170,
                "y": 25
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "8ef30f1c-18cc-470b-b4fb-db886fcb20f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 160,
                "y": 25
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "b4590f24-f8c2-454c-a0f4-317e55434d7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 149,
                "y": 25
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0a9938a4-7d43-4fc7-a030-530a06c25995",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 139,
                "y": 25
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "18690fe8-92b5-4023-a3c3-3c142d2f5fef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 129,
                "y": 25
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d0a58c25-84a8-48c3-a2e3-ee50f5427df8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 120,
                "y": 25
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c026f5df-3823-4084-9ee1-fb61bbb5536d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 210,
                "y": 25
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "1f2b6984-8f3e-422e-a8a6-45c69dc34ea8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 110,
                "y": 25
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d10a790d-324a-499a-b545-e5a3cedfea83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 90,
                "y": 25
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "990db67d-4fa5-47d1-8c98-738d4070a355",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 79,
                "y": 25
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "4061ba58-f08b-4b79-a4d7-30da8f137115",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 66,
                "y": 25
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "2632c97a-73de-4080-a32a-1928a36e0c8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 55,
                "y": 25
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "502d0874-43d1-40a8-bb09-316b16622fac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 44,
                "y": 25
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "58930ce5-cdc3-4b0d-8acf-1f1b16050c75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 34,
                "y": 25
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "962ee34c-a40d-488b-b8cb-dd7f0f080180",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 25,
                "y": 25
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "230e31ef-d5c4-40b3-acf1-c590ce22b1f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 21,
                "y": 25
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "1833c686-40ad-4051-b90f-d000701de786",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 12,
                "y": 25
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "6cf86f3a-6e3a-4f42-ad5f-ccf6b09e013b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 176,
                "y": 71
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "59d12391-00a4-49a0-96a3-3207d52e04ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 21,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 186,
                "y": 71
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}