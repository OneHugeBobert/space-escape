{
    "id": "2f02516d-8e73-479e-bb86-f7a775918171",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fTitle",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Palatino Linotype",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e91106dc-a48d-4aab-9d1b-b7492dbffaab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 180,
                "offset": 0,
                "shift": 33,
                "w": 33,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "7b669463-b709-4f56-a53a-54fe8f4f6365",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 180,
                "offset": 8,
                "shift": 37,
                "w": 22,
                "x": 376,
                "y": 366
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "08218616-c4fd-46dc-bce0-48d033823596",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 180,
                "offset": 7,
                "shift": 51,
                "w": 38,
                "x": 336,
                "y": 366
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "7876ee7c-61c0-4a4e-a76a-bd0704d95acb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 180,
                "offset": 5,
                "shift": 64,
                "w": 54,
                "x": 280,
                "y": 366
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "af4d778d-08e1-4778-bbbd-547dab65fc88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 180,
                "offset": 3,
                "shift": 67,
                "w": 60,
                "x": 218,
                "y": 366
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "8f634d09-7614-4fd6-b507-514a816bc7d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 180,
                "offset": 5,
                "shift": 118,
                "w": 109,
                "x": 107,
                "y": 366
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "e3aaaa37-32af-448d-92ca-cb8f3296d31b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 180,
                "offset": 6,
                "shift": 111,
                "w": 103,
                "x": 2,
                "y": 366
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "3f52c513-6088-4e14-8b80-c731be4aa3ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 180,
                "offset": 8,
                "shift": 30,
                "w": 14,
                "x": 1975,
                "y": 184
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "51fe2fe9-3f63-4481-a51d-1774a5c260b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 180,
                "offset": 8,
                "shift": 44,
                "w": 32,
                "x": 1941,
                "y": 184
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "9be6e85f-3f92-4786-b34c-eb94e5eb75af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 180,
                "offset": 4,
                "shift": 44,
                "w": 32,
                "x": 1907,
                "y": 184
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "93a7ea69-b702-43aa-aece-7a758fa06a84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 180,
                "offset": 5,
                "shift": 59,
                "w": 48,
                "x": 400,
                "y": 366
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "d441691d-fdf7-4a10-8038-4c33b2a30b81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 180,
                "offset": 2,
                "shift": 67,
                "w": 63,
                "x": 1842,
                "y": 184
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "1e847fa4-11d9-4cb8-bd77-00084fb34fe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 180,
                "offset": 0,
                "shift": 33,
                "w": 31,
                "x": 1749,
                "y": 184
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "6c310417-1690-4666-a3a9-ca4693c8646d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 180,
                "offset": 2,
                "shift": 44,
                "w": 41,
                "x": 1706,
                "y": 184
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "018bdbdf-04ba-4025-87b2-d737bd15592d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 180,
                "offset": 6,
                "shift": 33,
                "w": 21,
                "x": 1683,
                "y": 184
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "27188c37-fa00-4e22-82b3-678dda4c9399",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 180,
                "offset": -1,
                "shift": 39,
                "w": 42,
                "x": 1639,
                "y": 184
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "26c5247b-9347-4e94-b50a-7eefe30fa8f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 180,
                "offset": 4,
                "shift": 67,
                "w": 59,
                "x": 1578,
                "y": 184
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "a389265d-9354-4de3-b9d2-7edf2c81d2b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 180,
                "offset": 4,
                "shift": 67,
                "w": 57,
                "x": 1519,
                "y": 184
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "b0b5ef41-b1f6-43a6-b42d-13cb54e93408",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 180,
                "offset": 3,
                "shift": 67,
                "w": 60,
                "x": 1457,
                "y": 184
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "1f7d89c1-c343-4f7c-9c8f-731acafb0487",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 180,
                "offset": 3,
                "shift": 67,
                "w": 58,
                "x": 1397,
                "y": 184
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "33a18eca-bdc8-4971-946b-133182c5ca09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 180,
                "offset": 1,
                "shift": 67,
                "w": 62,
                "x": 1333,
                "y": 184
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "219d7cdf-28ee-422a-95cb-10940799ee68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 180,
                "offset": 3,
                "shift": 67,
                "w": 58,
                "x": 1782,
                "y": 184
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "70f191f7-4049-4e6c-aa44-07fbb1ef9b4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 180,
                "offset": 5,
                "shift": 67,
                "w": 58,
                "x": 450,
                "y": 366
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "cbda9d6f-9c4e-4016-995a-e208210b2186",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 180,
                "offset": 6,
                "shift": 67,
                "w": 60,
                "x": 510,
                "y": 366
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "7571a809-a810-482f-8242-4a78e5eb54d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 180,
                "offset": 4,
                "shift": 67,
                "w": 58,
                "x": 572,
                "y": 366
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "8964e0c1-8112-4564-8ad7-68dc9a9632cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 180,
                "offset": 4,
                "shift": 67,
                "w": 58,
                "x": 124,
                "y": 548
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e0231126-42fd-4464-88b5-194c0a17393a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 180,
                "offset": 6,
                "shift": 33,
                "w": 21,
                "x": 101,
                "y": 548
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "97fef757-977e-4c0e-b063-86fbcf032382",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 180,
                "offset": -4,
                "shift": 33,
                "w": 32,
                "x": 67,
                "y": 548
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "2bc0c143-8d4b-436a-9b02-60e85303e0f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 180,
                "offset": 2,
                "shift": 67,
                "w": 63,
                "x": 2,
                "y": 548
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "c678521f-5fa2-4a49-88a4-fa7622174ded",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 180,
                "offset": 2,
                "shift": 67,
                "w": 63,
                "x": 1954,
                "y": 366
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "a3c090d9-6ff4-40f4-b020-4756bec31e05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 180,
                "offset": 2,
                "shift": 67,
                "w": 63,
                "x": 1889,
                "y": 366
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "5bf4ea8d-8acf-4ae2-8f70-68c08cf3864b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 180,
                "offset": 5,
                "shift": 59,
                "w": 50,
                "x": 1837,
                "y": 366
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "4d8eb985-6b51-49ac-8bac-d986db065ab0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 180,
                "offset": 0,
                "shift": 93,
                "w": 92,
                "x": 1743,
                "y": 366
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "52101393-cdea-4f3f-b986-58dcf9d68805",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 180,
                "offset": 3,
                "shift": 103,
                "w": 98,
                "x": 1643,
                "y": 366
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "23a36005-af3e-4b57-b910-c1faf3911d8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 180,
                "offset": 4,
                "shift": 88,
                "w": 77,
                "x": 1564,
                "y": 366
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "8d04d877-5e39-4de3-a562-5156fafb5b1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 180,
                "offset": 6,
                "shift": 96,
                "w": 86,
                "x": 1476,
                "y": 366
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "e41b026e-b680-4bc8-ada0-cdf64c009fa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 180,
                "offset": 4,
                "shift": 111,
                "w": 101,
                "x": 1373,
                "y": 366
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "94569a08-4020-43ea-8ff4-224537b4c06b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 180,
                "offset": 4,
                "shift": 81,
                "w": 72,
                "x": 1299,
                "y": 366
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "e09d1726-4a65-42c2-bd81-b15a0c8c0bb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 180,
                "offset": 4,
                "shift": 74,
                "w": 66,
                "x": 1231,
                "y": 366
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "3d9301ef-9576-435b-9e60-9702aafd0d05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 180,
                "offset": 7,
                "shift": 111,
                "w": 98,
                "x": 1131,
                "y": 366
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "8442b275-2714-45f3-96bf-6a7f844e966f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 180,
                "offset": 4,
                "shift": 111,
                "w": 103,
                "x": 1026,
                "y": 366
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "a207d284-2bfe-4cf5-b7f9-c37ae01d0e44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 180,
                "offset": 5,
                "shift": 52,
                "w": 42,
                "x": 982,
                "y": 366
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "5d4572a5-f946-49a0-8b4b-f7790afaf3d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 180,
                "offset": 0,
                "shift": 52,
                "w": 49,
                "x": 931,
                "y": 366
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "9253b6ed-f1d6-4f46-9174-c34e3e848240",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 180,
                "offset": 4,
                "shift": 102,
                "w": 97,
                "x": 832,
                "y": 366
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "1b9767e3-0f79-4f5f-9c09-f938f4141fa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 180,
                "offset": 4,
                "shift": 81,
                "w": 72,
                "x": 758,
                "y": 366
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "62f16572-9974-489f-a58b-b28b97a2c94c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 180,
                "offset": 5,
                "shift": 133,
                "w": 124,
                "x": 632,
                "y": 366
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "08c4a338-915b-4174-a87c-5645370a1a00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 180,
                "offset": 5,
                "shift": 111,
                "w": 102,
                "x": 1229,
                "y": 184
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "c7eca22e-6127-4841-9a3c-9f30867bd1a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 180,
                "offset": 6,
                "shift": 111,
                "w": 99,
                "x": 1128,
                "y": 184
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "d24d745e-0c64-4903-92d8-9a0fc7b42ef8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 180,
                "offset": 4,
                "shift": 81,
                "w": 74,
                "x": 1052,
                "y": 184
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "92789ef5-7598-4774-8d51-0089727b52e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 180,
                "offset": 6,
                "shift": 111,
                "w": 99,
                "x": 1522,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "7d57b6fd-8361-4abf-9fc8-bb8a1a218cb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 180,
                "offset": 4,
                "shift": 95,
                "w": 90,
                "x": 1396,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "b55a8eea-2815-4008-9b6a-a090f59fe2b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 180,
                "offset": 7,
                "shift": 81,
                "w": 68,
                "x": 1326,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "df678d52-7f25-4631-b326-7269f31748b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 180,
                "offset": 2,
                "shift": 89,
                "w": 85,
                "x": 1239,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1709fc25-49bf-43f1-8fd5-33a6e03fb6fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 180,
                "offset": 3,
                "shift": 103,
                "w": 98,
                "x": 1139,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "7e44da34-811a-4675-b7bf-c04fc199ea38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 180,
                "offset": 3,
                "shift": 103,
                "w": 99,
                "x": 1038,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b601f9ed-4b49-4ab3-9a1e-e1ee0a466148",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 180,
                "offset": 2,
                "shift": 133,
                "w": 131,
                "x": 905,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "b90d22c7-2571-4adb-9a0e-a439d8cb804b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 180,
                "offset": 2,
                "shift": 89,
                "w": 85,
                "x": 818,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "e2e10993-0ac5-40ff-a281-13f3dad98fcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 180,
                "offset": 2,
                "shift": 89,
                "w": 86,
                "x": 730,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "48c20570-2002-4436-9b17-73c38036e44e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 180,
                "offset": 3,
                "shift": 89,
                "w": 81,
                "x": 647,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "fd61d9b0-29ef-4df3-949b-0347a882cdbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 180,
                "offset": 9,
                "shift": 44,
                "w": 32,
                "x": 1488,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "97e4d513-0250-45fb-91a8-647ac80b36d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 180,
                "offset": -2,
                "shift": 39,
                "w": 43,
                "x": 602,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "3c374f6e-5249-477b-a94c-46ae98dbf273",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 180,
                "offset": 3,
                "shift": 44,
                "w": 33,
                "x": 519,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "491e62a1-f674-46ed-9b8d-a31db0195c13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 180,
                "offset": 2,
                "shift": 67,
                "w": 63,
                "x": 454,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "23161984-b56f-4415-8b5c-e38df84834e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 180,
                "offset": 0,
                "shift": 67,
                "w": 67,
                "x": 385,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "147fc85a-7bf1-4c63-a22a-c31274e372ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 180,
                "offset": 9,
                "shift": 44,
                "w": 26,
                "x": 357,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a4495f08-cb09-407a-901e-73b9581f736c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 180,
                "offset": 5,
                "shift": 67,
                "w": 59,
                "x": 296,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "59590428-cff1-4141-aae5-9e0c15e270df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 180,
                "offset": 2,
                "shift": 81,
                "w": 73,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "2e320b10-f401-4e8e-b569-5424be36cadb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 180,
                "offset": 5,
                "shift": 59,
                "w": 51,
                "x": 168,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "15916a28-7f8c-429a-bd3e-08f89aa47e0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 180,
                "offset": 7,
                "shift": 81,
                "w": 71,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a3a464f4-7b5e-4ba9-973b-ae0c3562399d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 180,
                "offset": 6,
                "shift": 67,
                "w": 56,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "4496bf7e-6713-412d-a0bd-073e8b18e0ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 180,
                "offset": 5,
                "shift": 52,
                "w": 46,
                "x": 554,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "13a14faa-0f59-4beb-8779-886a38d81331",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 180,
                "offset": 5,
                "shift": 74,
                "w": 68,
                "x": 1623,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "ac8f739c-46d5-4b43-8d7a-3a37a4215e59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 180,
                "offset": 2,
                "shift": 81,
                "w": 77,
                "x": 264,
                "y": 184
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "800140a5-01aa-46ba-b4ba-4e461d22bff6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 180,
                "offset": 3,
                "shift": 44,
                "w": 38,
                "x": 1693,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "ff913803-2fcf-48d4-a484-05c3cc3a13f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 180,
                "offset": 0,
                "shift": 44,
                "w": 32,
                "x": 966,
                "y": 184
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "38bcc955-47c6-4111-b081-4753c90cd8d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 180,
                "offset": 3,
                "shift": 81,
                "w": 77,
                "x": 887,
                "y": 184
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "064a334b-39f1-4b9d-9188-d496d632401b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 180,
                "offset": 3,
                "shift": 44,
                "w": 38,
                "x": 847,
                "y": 184
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "54c818be-7fe3-4b24-bcdd-4adc2958e523",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 180,
                "offset": 3,
                "shift": 118,
                "w": 113,
                "x": 732,
                "y": 184
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a99618c4-f159-4483-95eb-d05ce8996591",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 180,
                "offset": 2,
                "shift": 81,
                "w": 77,
                "x": 653,
                "y": 184
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "2fb8aadf-2a7b-400c-90e7-1ddd2ad03f01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 180,
                "offset": 5,
                "shift": 74,
                "w": 64,
                "x": 587,
                "y": 184
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "c860464a-4167-4a42-ba56-cb680ab7bbb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 180,
                "offset": 3,
                "shift": 81,
                "w": 73,
                "x": 512,
                "y": 184
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "9087835c-a936-48c7-b814-b8c128cabe27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 180,
                "offset": 7,
                "shift": 81,
                "w": 72,
                "x": 438,
                "y": 184
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "72a72cfe-3af7-405f-ad48-245decd7354a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 180,
                "offset": 3,
                "shift": 52,
                "w": 49,
                "x": 387,
                "y": 184
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "1c041baf-273b-4f9a-bac1-2f9542aac433",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 180,
                "offset": 5,
                "shift": 59,
                "w": 50,
                "x": 1000,
                "y": 184
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e6558156-5ea7-4a82-9b46-5203328d6412",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 180,
                "offset": 2,
                "shift": 44,
                "w": 42,
                "x": 343,
                "y": 184
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "83d1d129-8d20-487d-827a-b7b167517f43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 180,
                "offset": 2,
                "shift": 81,
                "w": 76,
                "x": 186,
                "y": 184
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "9b35c510-500f-48fa-bc0d-9f3ff5367b04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 180,
                "offset": 1,
                "shift": 74,
                "w": 72,
                "x": 112,
                "y": 184
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "6e394b4c-8543-4dd8-8e38-27c423a04be5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 180,
                "offset": 2,
                "shift": 111,
                "w": 108,
                "x": 2,
                "y": 184
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "bbbca873-185b-4ae9-a24e-c834ea23fefd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 180,
                "offset": 2,
                "shift": 67,
                "w": 63,
                "x": 1965,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a2e48b93-6358-4464-9173-f60072f33a79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 180,
                "offset": 1,
                "shift": 74,
                "w": 72,
                "x": 1891,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "0bd386d4-ad7c-4b03-aa53-31f4b9a11a53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 180,
                "offset": 2,
                "shift": 67,
                "w": 60,
                "x": 1829,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "1065ccac-3d66-4fdc-a934-cbb7fb793ace",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 180,
                "offset": 0,
                "shift": 41,
                "w": 39,
                "x": 1788,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3e533e4a-f6dd-48f7-ab6a-1721f4f2903f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 180,
                "offset": 28,
                "shift": 67,
                "w": 11,
                "x": 1775,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f60da065-4698-48e3-ae3a-e7718d1c6abb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 180,
                "offset": 2,
                "shift": 41,
                "w": 40,
                "x": 1733,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "60dc0cef-b87a-456c-8b60-91b800cb7cd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 180,
                "offset": 2,
                "shift": 67,
                "w": 63,
                "x": 184,
                "y": 548
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "811cee52-bc94-46c1-b75b-6231a1f8011e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 180,
                "offset": 26,
                "shift": 129,
                "w": 77,
                "x": 249,
                "y": 548
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "99e31ae2-b239-4095-8993-2771de631a07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 45,
            "second": 84
        },
        {
            "id": "f3a86381-4abe-4bc3-852b-328509fafb3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 45,
            "second": 86
        },
        {
            "id": "df7ed775-95e8-42ae-b1da-b6696b914cd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 87
        },
        {
            "id": "9a5bfa39-4cfa-4ca3-9b90-5a3836c3ded7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 45,
            "second": 89
        },
        {
            "id": "a156bd87-77dd-419f-b3b8-6cd311cebdcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 65,
            "second": 84
        },
        {
            "id": "4de38798-fe43-4a69-aece-f5e1e21051c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 65,
            "second": 86
        },
        {
            "id": "3720782e-3fa1-40bd-a054-f60aaf195d3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 65,
            "second": 87
        },
        {
            "id": "017b5cf6-e434-4ce5-b1aa-6c9bdc3e1cd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 65,
            "second": 89
        },
        {
            "id": "b13b8ede-29a4-4482-a940-4827c1ebce73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 65,
            "second": 118
        },
        {
            "id": "07551022-0a75-4caa-bc3e-591880fdc374",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 65,
            "second": 119
        },
        {
            "id": "fcec9415-5577-4f01-9752-30cc6f4739d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 65,
            "second": 121
        },
        {
            "id": "ec4eb8de-dde6-4560-a814-e7fab9ce7553",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 65,
            "second": 8217
        },
        {
            "id": "a10d6007-5011-47a2-8ef1-44085b5222a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 65,
            "second": 8221
        },
        {
            "id": "cddb1a0c-348d-4dae-99e5-c8143fe0e32c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 70,
            "second": 44
        },
        {
            "id": "c65c815f-fa69-4131-b62b-5453177ff9bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 70,
            "second": 46
        },
        {
            "id": "76c0c286-6e10-4b20-ab06-c6addbb11e37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 65
        },
        {
            "id": "9bb56516-63d8-4ee5-b092-41cd68a40163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 76,
            "second": 84
        },
        {
            "id": "f69020a0-8d7f-40d6-ad08-1730eac99d67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 76,
            "second": 86
        },
        {
            "id": "f02c06db-4215-4a0b-81c5-d43231916cf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 76,
            "second": 87
        },
        {
            "id": "b5da1941-0cc8-4f52-817b-aa51e4f68071",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 76,
            "second": 89
        },
        {
            "id": "bf176754-bc1d-4a34-abcc-1969e9b498fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 76,
            "second": 121
        },
        {
            "id": "46922759-5ff2-4db7-8cdd-af85517ce3a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 76,
            "second": 8217
        },
        {
            "id": "45ad10a5-9bc1-4dad-bc43-637a511839d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 76,
            "second": 8221
        },
        {
            "id": "2ba44d41-149b-4f07-bd04-329b80385a5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 80,
            "second": 44
        },
        {
            "id": "6d8f6b88-f440-497c-b534-0e3b8ce208e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 80,
            "second": 46
        },
        {
            "id": "92754e28-87a7-4fc9-b6f9-ecafd717bcc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 80,
            "second": 65
        },
        {
            "id": "1a01b433-e1a6-43b0-8bb2-c74548393b32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 82,
            "second": 84
        },
        {
            "id": "3e6c8946-f4eb-45ae-88b4-c1b2e283c3d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 82,
            "second": 86
        },
        {
            "id": "3040c9be-2f20-4f13-aed8-62b257dc6b63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 82,
            "second": 87
        },
        {
            "id": "58ad4383-8cd0-4193-bd11-b0016785a689",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 82,
            "second": 89
        },
        {
            "id": "116a426d-e63f-421e-adce-a3b8d96b2d18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 82,
            "second": 121
        },
        {
            "id": "d7d70697-b4ed-4037-8093-0ad28f7aa6cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 84,
            "second": 44
        },
        {
            "id": "d493c241-5f34-40a4-887a-a21724a09c60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 84,
            "second": 45
        },
        {
            "id": "5e61a2e8-146d-4f0a-bbab-3e77ca0b2c24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 84,
            "second": 46
        },
        {
            "id": "d6110afe-741b-454f-b92f-b3c010b5f7da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 84,
            "second": 58
        },
        {
            "id": "79f01d19-14aa-4b8e-8654-52fb4402282c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 84,
            "second": 59
        },
        {
            "id": "4e9112c4-cbe7-42ca-8b6b-aa0008cb3090",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 84,
            "second": 65
        },
        {
            "id": "157afaf6-30ba-4f43-a9d4-c1effc3c98c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 84,
            "second": 97
        },
        {
            "id": "c74cb072-0c6f-4d42-870f-92b6311e0714",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 84,
            "second": 99
        },
        {
            "id": "bf7e050b-64b0-425e-a611-7b259207aa87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 84,
            "second": 101
        },
        {
            "id": "f3b11473-992e-4e12-939d-e277524a3fbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 105
        },
        {
            "id": "2e50cd65-2564-4d39-8b87-220c97e3d90f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 84,
            "second": 111
        },
        {
            "id": "c1c3bc57-49b0-4fce-9c42-415cf8153c3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 84,
            "second": 114
        },
        {
            "id": "b14cf8f3-5c46-4a02-82b4-ed3028e3edba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 84,
            "second": 115
        },
        {
            "id": "25eedf55-b1eb-4882-be27-c1ec024229cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 84,
            "second": 117
        },
        {
            "id": "fa189221-ec40-49b3-99cb-93f222104196",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 84,
            "second": 119
        },
        {
            "id": "8ed23021-bb03-4dc7-87ef-fe7bfcf1f4a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 84,
            "second": 121
        },
        {
            "id": "a1f2f82e-27e0-4f00-8e01-dc49177ec486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 84,
            "second": 173
        },
        {
            "id": "995a0643-76f5-4472-a412-106c591087c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 84,
            "second": 8208
        },
        {
            "id": "3b8d11e7-69a2-4275-b28c-64fc917f73a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 84,
            "second": 8209
        },
        {
            "id": "1e971d8f-c33d-4345-ae5a-95d7fcc01dfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 86,
            "second": 44
        },
        {
            "id": "07bd9c10-b33f-44c2-9d8b-635b81d37daf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 86,
            "second": 45
        },
        {
            "id": "c9511b6a-f748-4a27-9015-047ec150f531",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 86,
            "second": 46
        },
        {
            "id": "deab7f98-051f-43d6-bbd0-fc6c3c78725f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 86,
            "second": 58
        },
        {
            "id": "52024d19-4387-494d-b487-f7b7ccf5ca58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 86,
            "second": 59
        },
        {
            "id": "9a383e35-29b4-4886-b9f5-d6ccff59f535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -17,
            "first": 86,
            "second": 65
        },
        {
            "id": "a66f7431-aa95-42ed-9084-ea51b287bb17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 97
        },
        {
            "id": "208d8329-c375-4b80-8bda-3c798440a6c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 101
        },
        {
            "id": "3cabeb3e-a171-4015-bd93-2531ed37be74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 105
        },
        {
            "id": "aa007590-174f-41df-bf23-14fd6b68d55d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 86,
            "second": 111
        },
        {
            "id": "341ccb6a-d75e-4e7f-ab02-b1c4f79a0e9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 86,
            "second": 114
        },
        {
            "id": "9a452f3c-3803-481d-b658-ae953806bc3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 86,
            "second": 117
        },
        {
            "id": "65d38bca-2997-4007-bdc1-7011c8b7b15f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 86,
            "second": 121
        },
        {
            "id": "b4b9f3c7-98ba-4184-bc1c-a10aa5eed46c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 86,
            "second": 173
        },
        {
            "id": "caf74991-0839-479a-a77e-703f1791a66d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 86,
            "second": 8208
        },
        {
            "id": "8e4be011-d520-4eae-963e-9aae9086efe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 86,
            "second": 8209
        },
        {
            "id": "369339b1-ee69-4a62-9232-0e9a6bcc3cd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 87,
            "second": 44
        },
        {
            "id": "02059668-a198-483a-85a9-57b176fb73ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 87,
            "second": 45
        },
        {
            "id": "fbd208d4-32e5-4a86-bf8a-9ccd3b4753fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 87,
            "second": 46
        },
        {
            "id": "41110eca-6c9f-49c3-a846-81d4b5fd4af9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 87,
            "second": 58
        },
        {
            "id": "d8e9d44f-76fd-4155-9878-8a5f0a3b9cac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 87,
            "second": 59
        },
        {
            "id": "86b070d8-09e7-4e13-b0df-ffcf16a0e0a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 87,
            "second": 65
        },
        {
            "id": "da183b6f-884e-4969-903f-c54195d6ea9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 87,
            "second": 97
        },
        {
            "id": "f3103995-4d9a-4e1d-89c2-69b0b2eaef64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 87,
            "second": 101
        },
        {
            "id": "9fdf5252-6d30-4be4-8486-70bd59e66752",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 105
        },
        {
            "id": "9eead26c-0664-4ebe-bfbd-5755743d353d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 87,
            "second": 111
        },
        {
            "id": "59d9023d-be00-404a-b33f-cdee657e53d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 87,
            "second": 114
        },
        {
            "id": "25afc72a-9316-4a7a-9628-d304b2a947e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 87,
            "second": 117
        },
        {
            "id": "9eeb3f49-fd02-40c6-b2f5-7eea1d497096",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 87,
            "second": 121
        },
        {
            "id": "0f6b1f2c-8109-4e94-9add-915e19d42ce1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 87,
            "second": 173
        },
        {
            "id": "ff83a9e6-69c7-44dc-b1bb-0e2f442f9b53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 87,
            "second": 8208
        },
        {
            "id": "5b947021-c5bc-4ebe-8dc1-ba1a517414a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 87,
            "second": 8209
        },
        {
            "id": "2c80f8ee-743e-4001-9979-f24324a1581a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 89,
            "second": 44
        },
        {
            "id": "31cd9d26-38ea-4042-a532-50f6e02e65ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 89,
            "second": 45
        },
        {
            "id": "fe6e163f-638d-4ff6-bc1a-5b67f33c7b24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 89,
            "second": 46
        },
        {
            "id": "32c31d1b-fa55-46e5-8576-af42700b0d94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 58
        },
        {
            "id": "c3450f40-05f2-4123-a728-c28f825207e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 59
        },
        {
            "id": "7e27eca6-3dc8-4710-b569-eee6432f927a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 65
        },
        {
            "id": "77951b16-c97b-462d-bbdd-0ba56143b819",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 89,
            "second": 97
        },
        {
            "id": "580a19da-4da2-4f2a-b9fe-d00176902572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 89,
            "second": 101
        },
        {
            "id": "083d123f-85a0-474e-913c-c83e35192adc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 105
        },
        {
            "id": "623f18f8-8cf3-4eb9-a1b4-a49d1a29d5d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 89,
            "second": 111
        },
        {
            "id": "08471575-3623-4a8f-8840-af3adbc7c6bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 112
        },
        {
            "id": "3cd90707-ae0d-4650-aaca-87a1087b7111",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 89,
            "second": 113
        },
        {
            "id": "0cfcca84-b0a8-4535-b3df-12238eb2c6ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 117
        },
        {
            "id": "d1ac7862-fbf4-4f77-ab72-6a255b95b5c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 89,
            "second": 118
        },
        {
            "id": "279fb848-63e6-4102-9c13-ef02892217a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 89,
            "second": 173
        },
        {
            "id": "486b9b04-4f4a-457b-bdc9-561bb9a63f73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 89,
            "second": 8208
        },
        {
            "id": "c7d2302e-745e-433d-b1f4-09f8d8f9baa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 89,
            "second": 8209
        },
        {
            "id": "d8617383-73db-4540-a1b6-7a4e88966584",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 102,
            "second": 8217
        },
        {
            "id": "ad336154-8d45-4519-a581-ecd5824b5a16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 102,
            "second": 8221
        },
        {
            "id": "e288bee8-fb19-4c42-a850-f7b1d8a79873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 114,
            "second": 44
        },
        {
            "id": "3161577a-635c-4a28-8375-e677e7875bd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 114,
            "second": 46
        },
        {
            "id": "ec51ca52-51a2-45a4-96fb-0c9e2ac6fc3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 7,
            "first": 114,
            "second": 8217
        },
        {
            "id": "f8641c18-f299-4bd2-93eb-7a22dc407d0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 7,
            "first": 114,
            "second": 8221
        },
        {
            "id": "e8584fa6-2654-4bae-810c-2f4cbceb6d69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 118,
            "second": 44
        },
        {
            "id": "e5f399dd-3e00-4971-bafd-6dc95c343b01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -15,
            "first": 118,
            "second": 46
        },
        {
            "id": "6a83ddbb-91b3-4556-96fa-faf0044a545f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 119,
            "second": 44
        },
        {
            "id": "e1141bf0-f592-415d-b1b5-a5201e309644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 119,
            "second": 46
        },
        {
            "id": "04770e57-0b6c-414e-bec5-7576ad8ed181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 121,
            "second": 44
        },
        {
            "id": "c8c43692-3d17-4fb6-b0e1-e80eea296c8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 100,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}