{
    "id": "f063175f-7ab7-408a-93fc-d8f257d78ccb",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fMenu",
    "AntiAlias": 1,
    "TTFName": null,
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Palatino Linotype",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "c3727273-65c2-4caf-8cd5-5376fe7b2f96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 65,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3af8d62e-aa18-4f47-a7b6-71076e6daad2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 65,
                "offset": 3,
                "shift": 13,
                "w": 8,
                "x": 231,
                "y": 203
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a3b2122d-dcdc-4132-8e04-ad326683954c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 65,
                "offset": 2,
                "shift": 19,
                "w": 14,
                "x": 215,
                "y": 203
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "458e25b3-5e5c-4e1a-9539-ee2149e04c96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 65,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 193,
                "y": 203
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "8ace1fc4-51d7-4ea9-9e41-8a16e6c235dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 65,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 169,
                "y": 203
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "afa95cee-e99f-4205-97ad-69c936830501",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 65,
                "offset": 1,
                "shift": 43,
                "w": 40,
                "x": 127,
                "y": 203
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "4e64d416-98b3-4492-964f-3b9d80309f8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 65,
                "offset": 2,
                "shift": 40,
                "w": 38,
                "x": 87,
                "y": 203
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "e403e2d5-9fea-4e38-a1f8-7d9a5e4c3922",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 65,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 79,
                "y": 203
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "50784df1-51ab-4d64-8fbe-cffa14f39d20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 65,
                "offset": 3,
                "shift": 16,
                "w": 12,
                "x": 65,
                "y": 203
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f95bf7b3-0077-435f-933a-4eaded59c562",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 65,
                "offset": 1,
                "shift": 16,
                "w": 12,
                "x": 51,
                "y": 203
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "528bdee0-81cf-4698-8d1f-e53a7ce5ec06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 65,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 241,
                "y": 203
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "ea3253b7-9242-4cd9-8e97-5681ef181f20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 65,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 25,
                "y": 203
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "4f729393-a61e-4809-aeb2-7a321d6a7e4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 65,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 482,
                "y": 136
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "c826cf6c-3d8a-4d1b-be33-621f7642929c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 65,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 464,
                "y": 136
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7fbc7b9f-74ce-465f-b214-cfc5c33e191e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 65,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 454,
                "y": 136
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "66f85033-211c-4b40-885c-d8443a8c3e29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 65,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 436,
                "y": 136
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3fa5b777-8bc2-4f60-85b0-8d0896bf9eed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 65,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 412,
                "y": 136
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "606f2ead-3315-468a-a25f-9c0222672caa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 65,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 389,
                "y": 136
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "eec43d4e-934a-48dc-84ef-4f150a120a0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 65,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 365,
                "y": 136
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "3156f298-30fd-475e-8b3d-cacc61d9cbad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 65,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 342,
                "y": 136
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "8e632ace-ab52-4aad-880f-54bbb4038600",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 65,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 317,
                "y": 136
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "d48ea02b-a84f-4193-a6ed-df477de9dab2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 65,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 2,
                "y": 203
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "81dadafe-3f3a-4f6a-b863-8affc3257df1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 65,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 261,
                "y": 203
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "eff9ddd7-1a25-47e6-afa3-a0d7036ec63d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 65,
                "offset": 2,
                "shift": 24,
                "w": 22,
                "x": 285,
                "y": 203
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "a98f46ca-3552-4a47-88dd-ac475829ec2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 65,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 309,
                "y": 203
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "7a7b47f7-75e9-45fc-b9e8-5afaa01cb070",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 65,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 434,
                "y": 270
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e74ba850-cb24-47ed-8532-37740e7e8881",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 65,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 424,
                "y": 270
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "00f513d5-9dfd-4d2f-9b0c-047639782ed4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 65,
                "offset": -2,
                "shift": 12,
                "w": 12,
                "x": 410,
                "y": 270
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "ae21a05c-82a6-46a2-8ff4-3b52ab4cd73b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 65,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 384,
                "y": 270
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "6e13f466-0f88-4493-86d7-28643ffe221e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 65,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 358,
                "y": 270
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "7a8dd9f1-ec2f-4ffd-9b4d-22ac0ec33380",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 65,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 332,
                "y": 270
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "b2be6674-8fcb-4747-9c7a-05a661f63064",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 65,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 312,
                "y": 270
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "ddfd3a3d-bc2f-4565-bffb-bb87b045612c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 65,
                "offset": 0,
                "shift": 33,
                "w": 34,
                "x": 276,
                "y": 270
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e5039cc5-1a7b-4b19-a1bf-1ff9b95f226f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 65,
                "offset": 1,
                "shift": 37,
                "w": 36,
                "x": 238,
                "y": 270
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "0037ba7f-9bdc-4224-b0cd-f48023236660",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 65,
                "offset": 1,
                "shift": 32,
                "w": 28,
                "x": 208,
                "y": 270
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "96800b18-a7c4-426d-aef5-8c3d49aae1e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 65,
                "offset": 2,
                "shift": 35,
                "w": 32,
                "x": 174,
                "y": 270
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "2b0de552-9fa1-41ad-b5e0-4516798c8809",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 65,
                "offset": 1,
                "shift": 40,
                "w": 37,
                "x": 135,
                "y": 270
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "193bdb17-ca54-4944-90f7-dfc0981027c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 65,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 106,
                "y": 270
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "65306f22-7e84-4715-b8df-c6cf472185ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 65,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 80,
                "y": 270
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0c0dda17-5610-453c-9100-a370b2615a75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 65,
                "offset": 2,
                "shift": 40,
                "w": 36,
                "x": 42,
                "y": 270
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "8293faba-b5d4-409f-841b-72433ba1f11c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 65,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 2,
                "y": 270
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "1451b733-db70-44e0-9619-c75670828dd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 65,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 468,
                "y": 203
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "fb5282d0-1992-4732-bf20-b81c7a3aa875",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 65,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 448,
                "y": 203
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "5206af01-c099-4f7a-81dc-aa82e3042d3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 65,
                "offset": 1,
                "shift": 37,
                "w": 36,
                "x": 410,
                "y": 203
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "11c8a360-c01d-4b9b-a98b-68d7c43bda18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 65,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 381,
                "y": 203
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "dec65be2-d293-4bf6-9245-00e3e7bd5ab6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 65,
                "offset": 1,
                "shift": 48,
                "w": 46,
                "x": 333,
                "y": 203
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "d1a9fd82-fc20-4650-afe3-9d03e63ad296",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 65,
                "offset": 2,
                "shift": 40,
                "w": 37,
                "x": 278,
                "y": 136
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "49d4a4f6-f730-40fd-86ac-865e09b1b6de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 65,
                "offset": 2,
                "shift": 40,
                "w": 36,
                "x": 240,
                "y": 136
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "3f675263-f206-4a3c-a592-f92078b29dde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 65,
                "offset": 1,
                "shift": 29,
                "w": 28,
                "x": 210,
                "y": 136
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "98117ba8-3ed8-4f7f-9ddd-7d157caa6577",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 65,
                "offset": 2,
                "shift": 40,
                "w": 36,
                "x": 112,
                "y": 69
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "506a3c39-d52a-4eb9-ae6a-c3352cb68e0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 65,
                "offset": 1,
                "shift": 34,
                "w": 33,
                "x": 63,
                "y": 69
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "0373ee37-5c05-4330-85c2-36ed502651ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 65,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 36,
                "y": 69
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d09f31fb-1504-4c20-9de7-70aa993d0b25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 65,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 2,
                "y": 69
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "f801b582-8f5b-4d41-9200-214895afe371",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 65,
                "offset": 1,
                "shift": 37,
                "w": 36,
                "x": 445,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "5d5bbedf-2193-4ae0-9905-65c57d4b8425",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 65,
                "offset": 1,
                "shift": 37,
                "w": 36,
                "x": 407,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b3fb5a56-0517-48ef-b119-cec13f579177",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 65,
                "offset": 1,
                "shift": 48,
                "w": 47,
                "x": 358,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a62a0ebf-6a75-41b2-a08a-f4064ca94bfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 65,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 324,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "cc9cd5a7-0cde-40ad-898b-8f5037e5325b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 65,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 290,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "63be1016-03d9-4794-b234-3ccc814f7ed5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 65,
                "offset": 1,
                "shift": 32,
                "w": 30,
                "x": 258,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c5f48564-92ac-4592-b800-4425f3a20b39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 65,
                "offset": 3,
                "shift": 16,
                "w": 12,
                "x": 98,
                "y": 69
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "277b98f6-730a-444e-8187-82b358391568",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 65,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ef7961b6-1967-4ae0-a6c6-5040ec368519",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 65,
                "offset": 1,
                "shift": 16,
                "w": 12,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "2bb35180-83ca-46bf-96f4-7c33c2facbfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 65,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "3d6c1024-4580-4a93-bf5d-367154b2a7e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 65,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "4f37ab11-d1c5-442c-b368-ad231f994476",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 65,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "7be53dec-f3be-4354-9457-9aed6664bf1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 65,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "c0fe528c-e22c-4890-a15e-b1f2072dcbe9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 65,
                "offset": 0,
                "shift": 29,
                "w": 27,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "24a98890-3bd6-457c-910c-c9014a438b91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 65,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "18341b06-5416-442b-9872-6d84ee02d71e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 65,
                "offset": 2,
                "shift": 29,
                "w": 27,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "738ec65a-6f46-48be-a960-0496c66a05fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 65,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "45dee234-3e11-45db-97c9-b3a3781b68d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 65,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 220,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "90424d0b-01ab-4716-b840-91f683206641",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 65,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 150,
                "y": 69
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "89d565e2-21ca-4011-9108-b22d2d31f9e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 65,
                "offset": 1,
                "shift": 29,
                "w": 28,
                "x": 414,
                "y": 69
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a68700f3-dd1a-4d04-9a3d-05b845f0fd3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 65,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 178,
                "y": 69
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d6026f3f-3787-472e-8567-1573847def74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 65,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 176,
                "y": 136
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "1af97d3b-d159-40f7-a43b-e8e469780261",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 65,
                "offset": 1,
                "shift": 29,
                "w": 28,
                "x": 146,
                "y": 136
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "1d16709a-141a-4d41-8cb6-f426df642e94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 65,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 130,
                "y": 136
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "0563dbc6-9205-482a-9e05-800105d510f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 65,
                "offset": 1,
                "shift": 43,
                "w": 41,
                "x": 87,
                "y": 136
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "8aff2b9d-e6ae-4397-be9b-19a6f1e05aa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 65,
                "offset": 1,
                "shift": 29,
                "w": 28,
                "x": 57,
                "y": 136
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "94d09f03-3613-471d-883e-2991f074c7d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 65,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 31,
                "y": 136
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "a07aa8f8-8cbf-4d26-927a-732fcfe5478e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 65,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 2,
                "y": 136
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "3e9c32e3-f8bd-47eb-a566-149a28dcb3ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 65,
                "offset": 2,
                "shift": 29,
                "w": 27,
                "x": 481,
                "y": 69
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "5566f91a-1038-4f2b-a258-f911655c8cec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 65,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 461,
                "y": 69
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c34e88ca-d69e-4261-a6d1-426693225caa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 65,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 190,
                "y": 136
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "45e16ebc-f020-4c43-8aec-64c6c9c84cef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 65,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 444,
                "y": 69
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "3b6d9fbb-9681-4351-b351-6b2414fc2a4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 65,
                "offset": 1,
                "shift": 29,
                "w": 28,
                "x": 384,
                "y": 69
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "4df446a6-b13b-4fdb-802e-56a37d2e5d62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 65,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 355,
                "y": 69
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c1bd5697-d2f2-4633-be33-bcff4e913b06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 65,
                "offset": 0,
                "shift": 40,
                "w": 40,
                "x": 313,
                "y": 69
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a32de3e2-1d2e-4f3f-be7a-36260c7b59f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 65,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 288,
                "y": 69
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d9558a1f-6990-4d8f-9be5-6e9410233a1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 65,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 259,
                "y": 69
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "ef80bce1-ac54-4814-9ecc-374fd2f76a18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 65,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 234,
                "y": 69
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "8402a30b-5192-4c3c-bbea-d73745544bc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 65,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 217,
                "y": 69
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c34466b0-683c-449a-b227-8908710fbf84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 65,
                "offset": 10,
                "shift": 24,
                "w": 4,
                "x": 211,
                "y": 69
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "c64ea356-f6e9-4c29-b971-16b435a62779",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 65,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 194,
                "y": 69
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "24d508a8-f0eb-4c45-bcfb-76739908d864",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 65,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 458,
                "y": 270
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "a5a01f59-2eed-4ce4-b6f6-84cc3e7607c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 65,
                "offset": 9,
                "shift": 47,
                "w": 28,
                "x": 2,
                "y": 337
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "c0f024e8-1a34-4b24-9f8e-3d260e8ca8a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 45,
            "second": 84
        },
        {
            "id": "036af7e1-166c-4e15-95f1-1317a7b60cd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 45,
            "second": 86
        },
        {
            "id": "09cd33fd-0cde-4f82-8a75-ad024ee13e47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 87
        },
        {
            "id": "3278565d-211b-4ac9-84d7-4b21dcd89472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 45,
            "second": 89
        },
        {
            "id": "345b28a4-5096-4a24-83db-08526f2fbab9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 84
        },
        {
            "id": "0e3a1192-a944-4740-ab61-ff7a184c7870",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 86
        },
        {
            "id": "39aa1d77-2e6f-407a-a864-bc2e117da8c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 87
        },
        {
            "id": "7be68afe-1986-4b53-95ce-9925dd110ae2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 89
        },
        {
            "id": "a434b247-a0d4-4848-91b3-385eeb34a18c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 118
        },
        {
            "id": "a9490ef4-9b52-4a6c-bc74-ae0ccbc92958",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 119
        },
        {
            "id": "19821100-ee47-4144-9494-c5f74ccc1890",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 121
        },
        {
            "id": "b4c49def-e529-47c9-b1ab-c740b7aec401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 8217
        },
        {
            "id": "30c87526-9bc4-45ea-b21f-494ae1651c1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 8221
        },
        {
            "id": "6cf3c40b-94a5-4969-b9d0-bf233475ce28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 44
        },
        {
            "id": "7465ab47-af2f-4148-a248-47dc1c0363cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 46
        },
        {
            "id": "ea869bdc-d0c6-4c15-8a25-69ede240135c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 65
        },
        {
            "id": "3403725f-b4fe-42c9-a98f-8cac5c92f8dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 84
        },
        {
            "id": "9312f2fb-57fe-47ac-ae79-a49d26eb3d9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 86
        },
        {
            "id": "965003ac-234e-4569-b01a-080f4567700d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 87
        },
        {
            "id": "31a1a9af-bf08-4116-8a4d-1f498e812c75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 89
        },
        {
            "id": "7f87842d-d4ae-4626-a240-5649fde278a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 121
        },
        {
            "id": "d75fed50-e457-4cf6-b91b-d2ca1cf6883c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 8217
        },
        {
            "id": "ff0dc644-6073-4cd8-bf6d-c52eab67cd1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 8221
        },
        {
            "id": "b8f78d97-90f3-43a4-882a-e9126387bd8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 44
        },
        {
            "id": "3f9795bc-6b5c-42a3-b5d2-20607f33a233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 46
        },
        {
            "id": "f7b1fcd8-c03d-4723-a19f-d696652aa1b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 65
        },
        {
            "id": "50da9731-0304-42f1-b340-a5774d1f8a74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 84
        },
        {
            "id": "bb20770f-1c6e-47c9-8307-cedc4f3bd620",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 86
        },
        {
            "id": "64f35357-2438-4f33-8d58-7cf26776907b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 87
        },
        {
            "id": "048ad22b-bea6-4a1e-93a3-78e009647039",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 89
        },
        {
            "id": "8aad5f0b-b7aa-4407-88f9-fd2d81abc14d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 121
        },
        {
            "id": "8a773210-3029-4de4-b8fa-5bd94952891d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 44
        },
        {
            "id": "6f02e0f8-545d-4281-bec9-bab2fa5435e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 45
        },
        {
            "id": "942bb7af-99d8-485a-b63c-878234cdb320",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 46
        },
        {
            "id": "077f3562-0717-4892-8b6d-7f1b38200333",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 58
        },
        {
            "id": "b4c796de-2929-4933-83fa-fc5d41bf1715",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 59
        },
        {
            "id": "c78863ff-088d-4b8f-9c68-e0aa1d7cf3ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 65
        },
        {
            "id": "3ccd82d1-6cf4-4c06-8632-1dfc4c472a9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 97
        },
        {
            "id": "9308fa29-856c-4118-a2a7-6ce87853969a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 99
        },
        {
            "id": "231ccbd0-b0f6-44c6-bce5-37e830d4b68a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 101
        },
        {
            "id": "4e143b09-060e-4908-8ac4-56c96da2754a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 105
        },
        {
            "id": "e4c6b3c1-470f-45df-af53-d46fb68cc8ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 111
        },
        {
            "id": "4a6040ea-2f2b-4706-8f80-a339ca81cc8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 114
        },
        {
            "id": "4925230d-4f7c-4755-bcb1-d396a1d000e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 115
        },
        {
            "id": "29f46de0-f6f2-4d00-be80-dc5e6c49b165",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 117
        },
        {
            "id": "167cff9d-5171-4b47-adbd-ede719842e66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 119
        },
        {
            "id": "932277ca-4ec9-4d4e-91d2-153e056f56ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 121
        },
        {
            "id": "1d9fd090-ea43-4108-9f6b-7ad9be6f5b5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 173
        },
        {
            "id": "332b4c6a-3b95-4a7b-87e4-47bccd3ae947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 8208
        },
        {
            "id": "cd3ed556-857c-405c-944b-df81de566284",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 8209
        },
        {
            "id": "8b7e8df6-be07-404f-8c0e-afacf3f3d7f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 44
        },
        {
            "id": "649c88b6-41cc-4cd4-b7a6-bbe5c2bdf6d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 45
        },
        {
            "id": "72438391-d7b3-43f3-8a8a-215c542aedd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 46
        },
        {
            "id": "e829999f-41ba-47a1-a57e-1616b216d79b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 58
        },
        {
            "id": "c2b83735-ca5a-4305-92b2-add5d7776655",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 59
        },
        {
            "id": "b27c8f7e-0a63-412a-b566-f094675544b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 65
        },
        {
            "id": "0b40ce46-3711-4ddc-b789-0246f3941ae0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 97
        },
        {
            "id": "c56035dd-7b5b-47fc-a5de-41f31508b9a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 101
        },
        {
            "id": "8d312bda-706b-4e97-9566-2961c068258e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 105
        },
        {
            "id": "0d81a6f1-8e51-41dd-bded-8ec886cbf516",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 111
        },
        {
            "id": "da2ae962-c76a-4ed8-b927-df73b6d15598",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 114
        },
        {
            "id": "49ce344f-0b23-4471-a0da-cc5296cba943",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 117
        },
        {
            "id": "4fd9cea9-d70f-4d51-bf5c-f97e83a2447b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 121
        },
        {
            "id": "b2d124af-e3a4-488d-a622-53b833f63f78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 173
        },
        {
            "id": "89cec320-7a61-4592-8756-e5a6296e7ae5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 8208
        },
        {
            "id": "4bec58bd-6e61-45f1-91b4-9b34ffdd7a88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 8209
        },
        {
            "id": "ad06e68d-d8ce-420f-a9b7-91ca597aaa30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 44
        },
        {
            "id": "262b5806-b420-47c3-922e-9315cafefd3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 45
        },
        {
            "id": "b8cbf359-43a6-4de9-baba-126a13f0eea3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 46
        },
        {
            "id": "d19411ac-3c93-425a-a636-72e9341a7246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 58
        },
        {
            "id": "72e2432c-29bd-4623-bd54-0b8033098a46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 59
        },
        {
            "id": "f3366bdc-ef81-406a-9a85-a018efb4ba05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 65
        },
        {
            "id": "8f389bab-6c85-47f1-b4c3-e33dbf6a46c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 97
        },
        {
            "id": "2bed29e1-25d1-40d7-affe-2d213aa77f88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 101
        },
        {
            "id": "037b6216-40a0-4e41-ac30-ed41fb9cf736",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 105
        },
        {
            "id": "79bcdfc8-ff30-4b7e-952b-fc1649927046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 111
        },
        {
            "id": "6b3e4f9e-e01e-4ff9-a6d0-bee840689502",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 114
        },
        {
            "id": "c5bf393a-1ff4-402d-b4b1-ebdd25acf785",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 117
        },
        {
            "id": "730545e1-0bf2-4da6-8c32-51903068c262",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 121
        },
        {
            "id": "c08e09fa-1ddf-485e-adfa-1c612f00fb4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 173
        },
        {
            "id": "79ef3c54-0489-4928-9884-ae874c29a4df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8208
        },
        {
            "id": "2b6837b8-afbc-4ad2-bcaa-d111efd830ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8209
        },
        {
            "id": "fec9e78d-be08-4eb0-9ff2-ca83edab7986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 44
        },
        {
            "id": "076ef0e5-141b-4b40-908c-d37cad343e16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 45
        },
        {
            "id": "ad6c5b45-dfbc-490f-851f-91decfd26d9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 46
        },
        {
            "id": "03623161-231e-414c-90be-125adde59bf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 58
        },
        {
            "id": "06cb3af3-445a-4605-a932-3ef6acfb84dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 59
        },
        {
            "id": "de47ce04-81de-41da-a3a3-68e9aa0dd101",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 65
        },
        {
            "id": "a7edc76a-9ed1-4200-9b92-f8c6b032593e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 97
        },
        {
            "id": "2b465bc2-1edc-48a9-a456-bc12025d3591",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 101
        },
        {
            "id": "00dfcf1f-4854-4fdd-8fb7-1730a2e4e7d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "96dd4054-820d-41b5-b7f1-b8a2b45aec9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 111
        },
        {
            "id": "93e3e654-1abd-4106-b7a0-551e73c3a97e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 112
        },
        {
            "id": "ffb399b9-1680-4e0d-bb82-7f4ef3c6f838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 113
        },
        {
            "id": "7bc5c6b5-355f-4eb5-991e-afff6d8a5ace",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 117
        },
        {
            "id": "51b09bff-1348-4285-9591-26a5985d6d59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 118
        },
        {
            "id": "e656159b-f721-4f69-b28e-3efaa1ed55f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 173
        },
        {
            "id": "d4d3b674-cfe3-4208-b34c-1d9d7d5e58f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 8208
        },
        {
            "id": "8f341785-5711-4464-aaa9-ed3894edd8df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 8209
        },
        {
            "id": "ee8f96fc-d3bb-48be-adff-cd9e8d97ffa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 8217
        },
        {
            "id": "66698eb8-6b53-49b0-9187-4292ab4299ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 8221
        },
        {
            "id": "89f8eabb-6617-4897-9fa1-9fc1a19bb2e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 44
        },
        {
            "id": "607dce58-fc53-43d9-9ded-4eada1741874",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 46
        },
        {
            "id": "ed595339-edc1-45c1-ab28-3fc08c005e85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 114,
            "second": 8217
        },
        {
            "id": "3b3daa2f-5c9c-480b-ad58-8ea168cd3b49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 114,
            "second": 8221
        },
        {
            "id": "f30468dc-f32a-457e-9051-dc2c863c8d22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 118,
            "second": 44
        },
        {
            "id": "7ccf21ff-b3d3-4165-a1d9-c532287ed191",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 118,
            "second": 46
        },
        {
            "id": "21e3cd37-e11d-4db0-9252-4042037a64e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 44
        },
        {
            "id": "905c6f03-ca46-4ed9-90fc-82b4b8e8116a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 46
        },
        {
            "id": "b815a1b3-e47c-4884-968f-f86ce1a39499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 44
        },
        {
            "id": "b8add0ea-2aa7-455a-a1f3-feb3084c5118",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 1506,
            "y": 1506
        },
        {
            "x": 9647,
            "y": 9647
        },
        {
            "x": 65533,
            "y": 65533
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 36,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}