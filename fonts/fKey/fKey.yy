{
    "id": "e4190870-d997-4208-b14b-3924b12e6654",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fKey",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Droid Sans Mono",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b914f597-dfba-4ad6-9b73-fad1ef879cf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 32,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "103f8444-992c-4a05-a4d9-056e03fb008f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 32,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 179,
                "y": 104
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d1a76ff8-da8b-4847-843e-0be48046e58b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 32,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 168,
                "y": 104
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "5fdf8234-9d33-415f-869d-f95f9c5d2038",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 32,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 152,
                "y": 104
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "245d3ea4-2f49-4df0-8254-06fb43385297",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 32,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 139,
                "y": 104
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "7fe6abc5-3d2e-4e06-8a68-cabb673c91f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 32,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 122,
                "y": 104
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "0f81bb12-3a63-47d5-a669-4c02aa81bd12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 32,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 105,
                "y": 104
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "6f12c043-2b91-4a52-bf48-cfdbc3cd805c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 32,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 99,
                "y": 104
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "01d16089-ed25-49fa-80c9-abd577558dd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 32,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 89,
                "y": 104
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "9c7880d3-fa9e-4234-af4a-b59ef5071a91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 32,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 79,
                "y": 104
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "efc4c353-b509-498d-9724-36ec1c466546",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 185,
                "y": 104
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "87e7c918-c1e3-4f85-91c1-d20cd9cb250a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 65,
                "y": 104
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "d3a2c08a-eabf-41aa-8733-c4fafdefe574",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 32,
                "offset": 5,
                "shift": 14,
                "w": 5,
                "x": 45,
                "y": 104
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "968234dc-cc18-414f-ba3b-b616453daeb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 32,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 34,
                "y": 104
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "a8bac6df-e6b4-4398-bbe2-060d463ac5d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 32,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 28,
                "y": 104
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d5d98a02-c7e3-4b29-b0b5-af586043d876",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 32,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 16,
                "y": 104
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "0a72ff62-36c8-4c01-b996-49cf43acf1b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 104
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "1fdad996-8130-4f86-9a9f-93b029832e39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 32,
                "offset": 2,
                "shift": 14,
                "w": 7,
                "x": 239,
                "y": 70
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "b871c843-ab81-4173-8113-9ce872234b6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 225,
                "y": 70
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d2ef3bed-4ac9-440f-b5db-cb06dffd722a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 211,
                "y": 70
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a3188842-912b-4d8f-b862-55bb8bb69840",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 32,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 195,
                "y": 70
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "0e785c88-d18a-4590-8b25-44e59017414d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 32,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 52,
                "y": 104
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ffb04074-4227-40f2-a9a1-6ab208b81b0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 199,
                "y": 104
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e9cb0b9c-73dd-40a0-b108-681e7c57cb9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 213,
                "y": 104
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "4ef70bba-299f-470d-9083-d68c9d2f8ea8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 227,
                "y": 104
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "7940cc90-4b64-4bb6-8f3a-b3216e71b147",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 29,
                "y": 172
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "835a48f2-64fc-4423-ae9b-8e46502101d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 32,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 23,
                "y": 172
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "132b70f3-fa2f-4c81-b9b5-d670c1ff1859",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 32,
                "offset": 4,
                "shift": 14,
                "w": 5,
                "x": 16,
                "y": 172
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "94e1941e-6599-44e6-8c3b-43225f04fb79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 172
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "e86eb93b-4676-47df-bb38-7cbe7cc7e275",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 231,
                "y": 138
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "49a357e1-d075-468d-b335-991d1be5009c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 217,
                "y": 138
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "9ebbe54d-ed95-4135-9fc9-f6ef57fc209a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 203,
                "y": 138
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "1af564f0-ad9a-41cc-824d-6a690e370ffd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 32,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 186,
                "y": 138
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "ed061efe-1b26-41f6-84d3-b870c92ffdc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 32,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 169,
                "y": 138
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "52e816b8-f1ef-4bf1-ace4-23de61854b80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 154,
                "y": 138
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "42cf7896-a1dd-4852-8a8b-5bac96a008f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 139,
                "y": 138
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "24264999-bc65-404d-b71d-628d7cccde4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 124,
                "y": 138
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "1d947965-17a3-4001-8590-df1868122536",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 32,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 111,
                "y": 138
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "0bfa8d55-4b41-43f3-a8a4-78688a0de6cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 32,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 98,
                "y": 138
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "b1758fa5-7033-456d-b2eb-f468aa644a52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 84,
                "y": 138
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6e6cb5cc-5061-4a4b-9169-8e54bd9f7ec0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 70,
                "y": 138
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "08f78c4c-4e0a-47fa-8c72-768655bd03f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 32,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 58,
                "y": 138
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "5ddacb5d-fc7c-4829-a426-9b278a73f90b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 45,
                "y": 138
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "a8ecb2fe-5c4b-4487-b784-6a730276c322",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 32,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 30,
                "y": 138
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "8c35b84d-011d-467d-be1f-034c71e0d7bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 32,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 17,
                "y": 138
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "4321c89a-451e-4863-b1ca-68299e1ec936",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 138
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "4d8495ac-8cf9-47c0-ba38-461313ee6df1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 181,
                "y": 70
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d1b46436-35fc-40a7-a1df-e7713ea0a1aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 32,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 165,
                "y": 70
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "f56e800b-6c2f-436b-adef-a212b11b563c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 32,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 152,
                "y": 70
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "2013f3d9-9cd9-413b-88c4-fe1b5e77684d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 32,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 69,
                "y": 36
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "7d26ffb6-0a5b-4819-a35c-89dc38ec7e38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 32,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 45,
                "y": 36
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "fb62a606-29f5-4c56-bcab-7db304d947f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 31,
                "y": 36
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "2a68e517-910f-4b9a-a6e3-16424ec6335e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 16,
                "y": 36
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "cd34e89a-ee79-4bbd-84b1-b092d9dc1d2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "a0ef0c52-e196-42b4-a6bb-117b71b51236",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 32,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "ea4645d3-cce4-4dde-8bbf-fd6d31496808",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 32,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "94d21ed2-c96e-4deb-91ee-e8f0f5918e07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 32,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ffa6b9f5-5dca-4f7f-be20-2dc2343ffd54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 32,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 179,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4e79d613-fd7e-4e32-bbcb-6be15bef37df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "2279195b-2dc1-4f5d-8524-d86bf2da7ded",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 32,
                "offset": 4,
                "shift": 14,
                "w": 8,
                "x": 59,
                "y": 36
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1c6b267a-6e50-499f-8a6d-fe6337b330d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 32,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 152,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "267b0ec9-c1fb-4f51-8995-1dea65ec6746",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 32,
                "offset": 3,
                "shift": 14,
                "w": 7,
                "x": 128,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "e76e8466-d6be-4b1b-8794-201e69d7a3f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "faa03f43-7612-4d3c-a28f-2f718524d371",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 32,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "3c2c16a3-d0a1-4fc0-a2de-2d58fc62b1a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 32,
                "offset": 4,
                "shift": 14,
                "w": 6,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "dcc7c25a-4d29-48fe-aa3e-c04e621149f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "0048f4e9-6c2a-404b-88b3-6477ea8949bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "8aa01405-2ba4-44d4-b437-58f1eb40e3d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 32,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b60b7278-1e74-41d4-acf5-3c0027034d37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ee6796a7-228a-4374-9e28-6648c9173816",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "c20b8e36-8381-4318-a824-93dfcbc88419",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "981c3360-d79a-4fe0-9cbf-c907686e532f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 85,
                "y": 36
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f58d472c-28d8-4a29-ab15-7544955384f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 234,
                "y": 36
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "df1e73fb-42a8-4f8e-8a19-a974305453c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 32,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 100,
                "y": 36
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "19b04cb4-16c3-473f-8494-b61af4b7e049",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 10,
                "x": 127,
                "y": 70
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "acb95ddd-5f95-466b-a06c-b38ef7b53997",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 32,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 113,
                "y": 70
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "49d37099-41ed-4b30-a937-f80c4b6505a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 32,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 100,
                "y": 70
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "8ae4beb4-2ba1-4c66-89a0-4fd18a1197ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 85,
                "y": 70
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "5acc2922-5e21-4b0e-b521-93de1404864f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 71,
                "y": 70
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "78f8c708-2b96-474d-ab1d-5c381c4bf9b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 56,
                "y": 70
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "8407cc28-3a79-4592-b181-c9785bbc1598",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 42,
                "y": 70
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "65c686fd-f897-4e00-9a34-0c3d82023388",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 28,
                "y": 70
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e6522375-c8fa-45e8-be8b-4fdfa5e6e082",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 32,
                "offset": 3,
                "shift": 14,
                "w": 10,
                "x": 16,
                "y": 70
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "23648d45-aa7d-4441-be5b-117cb0fbc8de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 32,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 139,
                "y": 70
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "b33c7065-2a0c-4a06-80bd-e81a4a60163e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 70
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "6b89cd29-de56-4763-83b0-e2b48978afb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 220,
                "y": 36
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "dd894590-2b1c-4500-b512-d86029a1943d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 32,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 204,
                "y": 36
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "75d2fab6-2597-4e6f-b7e6-22996cf0eb6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 32,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 186,
                "y": 36
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "2e2e6490-6afd-4242-b122-0fa2aa3cf652",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 171,
                "y": 36
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "983db753-3c28-4a53-b4c0-aa4027a50b17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 32,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 155,
                "y": 36
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "2ebd5c67-7c2c-49ab-81e7-cd14f98d97a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 32,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 142,
                "y": 36
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "266a58e4-dc1d-4b39-8e27-2e79bcc0a7f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 32,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 130,
                "y": 36
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "ab65309c-c54b-467b-a61a-4007d778e0a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 32,
                "offset": 6,
                "shift": 14,
                "w": 3,
                "x": 125,
                "y": 36
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "65478be1-5e43-4096-92e7-e0ff087464cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 32,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 113,
                "y": 36
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "75bc1b02-65e3-4386-8339-34dd9c5815b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 43,
                "y": 172
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "591a4a8a-b295-45a5-b3eb-2aefa7e8f9e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 32,
                "offset": 4,
                "shift": 23,
                "w": 15,
                "x": 57,
                "y": 172
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 18,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}