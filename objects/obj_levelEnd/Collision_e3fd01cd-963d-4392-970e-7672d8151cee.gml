/// @description Move to next room

with (obj_player)
{
	if (hascontrol) && (haskey)
	{
		hascontrol = false;
		if room != 4
		{
			audio_play_sound(s_door,10,false);
			SlideTransition(TRANS_MODE.NEXT);
			x = x-5;
			y = y;
			haskey = false;
		}
	}
}

if room == 4
{
	if place_meeting(x,y,obj_player)
	{
		with(obj_player) if haskey
		{
			if canpause audio_play_sound(s_win,10,false);
			with (obj_player) canpause = false;
			with(obj_player) endgame = true;
			FreezeGame();
		}
	}
}