{
    "id": "34188c5a-4d8f-4b25-aa06-190df4870810",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_levelEnd",
    "eventList": [
        {
            "id": "e3fd01cd-963d-4392-970e-7672d8151cee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f1e15640-ba9e-46f8-8f45-25a8753fbe1e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "34188c5a-4d8f-4b25-aa06-190df4870810"
        },
        {
            "id": "228e93e8-189a-4093-a06c-53aafa4f2f4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "34188c5a-4d8f-4b25-aa06-190df4870810"
        },
        {
            "id": "67695040-88f6-4339-8fe8-0892286df99e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "34188c5a-4d8f-4b25-aa06-190df4870810"
        },
        {
            "id": "35f29eec-b2d6-4721-9929-ac3e3ed87add",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "34188c5a-4d8f-4b25-aa06-190df4870810"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "17f3a276-5af4-4dde-9ac9-b5dd9130ca26",
    "visible": true
}