{
    "id": "e4970365-5c46-4d12-a23a-c8c9cfd76611",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gun",
    "eventList": [
        {
            "id": "60e95e25-4bb8-4905-9cf1-f09f8905d0e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "e4970365-5c46-4d12-a23a-c8c9cfd76611"
        },
        {
            "id": "06eef780-0275-4e51-b155-42711b52d564",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e4970365-5c46-4d12-a23a-c8c9cfd76611"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3be78492-13a6-41f0-b896-c4837f756fdd",
    "visible": true
}