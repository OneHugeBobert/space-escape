//gun position
x = obj_player.x;
y = obj_player.y+10;

//gun points toward mouse cursor
if canfire image_angle = point_direction(x,y,mouse_x,mouse_y);

firingdelay = firingdelay - 1;
recoil = max(0,recoil - 1);

//Firing gun
if canfire
{
	if (mouse_check_button(mb_left)) && (firingdelay < 0)
	{
		firingdelay = 5;
		recoil = 4;
		ScreenShake(2,10);
		audio_play_sound(s_shoot,5,false);
		with (instance_create_layer(x,y,"Bullets",obj_bullet))
		{
			speed = 25;
			direction = other.image_angle + random_range(-3,3);
			image_angle = direction;
		}
	}
}

//Shows recoil when firing
x = x - lengthdir_x(recoil,image_angle);
y = y - lengthdir_y(recoil,image_angle);

//Flips gun when facing different direction
if (image_angle > 90) && (image_angle < 270)
{
	image_yscale = -1;
}
else
{
	image_yscale = 1;
}