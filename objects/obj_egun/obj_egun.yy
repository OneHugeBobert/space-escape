{
    "id": "6ced42ff-a61b-4aba-86d6-a9ad208b8d4c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_egun",
    "eventList": [
        {
            "id": "d6ed34fe-b952-418e-ba8e-8a361e9d9f05",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6ced42ff-a61b-4aba-86d6-a9ad208b8d4c"
        },
        {
            "id": "a0df2e49-638e-46ea-b698-ff42c09510b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "6ced42ff-a61b-4aba-86d6-a9ad208b8d4c"
        },
        {
            "id": "6f1301f9-a7b9-4480-9fd5-3a4ad6c9d371",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 7,
            "m_owner": "6ced42ff-a61b-4aba-86d6-a9ad208b8d4c"
        },
        {
            "id": "cae01515-4b18-4281-86c7-7cdfd93ef624",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 50,
            "eventtype": 7,
            "m_owner": "6ced42ff-a61b-4aba-86d6-a9ad208b8d4c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3be78492-13a6-41f0-b896-c4837f756fdd",
    "visible": true
}