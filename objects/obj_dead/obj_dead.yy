{
    "id": "47cee68e-053b-4998-af9a-a7b1a9f1754f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dead",
    "eventList": [
        {
            "id": "c8a81d4d-28c0-451a-a352-e2cf536c7465",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "47cee68e-053b-4998-af9a-a7b1a9f1754f"
        },
        {
            "id": "9cbacafd-a2fd-4fa7-86f5-ad4bc1a68868",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "47cee68e-053b-4998-af9a-a7b1a9f1754f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d29763be-471d-47ae-86b9-c297beaba78d",
    "visible": true
}