{
    "id": "3044cb10-c4aa-480d-b4ce-b00187120d5b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_key",
    "eventList": [
        {
            "id": "fe0d8116-7bb5-4385-9e50-1bd81e48f1c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f1e15640-ba9e-46f8-8f45-25a8753fbe1e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3044cb10-c4aa-480d-b4ce-b00187120d5b"
        },
        {
            "id": "b64fe5b1-cc68-4ce3-b2e1-5b7d1edae929",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3044cb10-c4aa-480d-b4ce-b00187120d5b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "d00843d3-63cf-4da7-802c-bbb837c3916d",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "Health",
            "varType": 3
        }
    ],
    "solid": false,
    "spriteId": "23863afa-48d1-489f-9c48-e2efdcb6e1bc",
    "visible": true
}