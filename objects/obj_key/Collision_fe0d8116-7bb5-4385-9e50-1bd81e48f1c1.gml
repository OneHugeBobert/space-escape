/// @description When player grabs key it disappears

audio_play_sound(s_select,10,false);
instance_destroy();

if Health = true
{
	with(obj_player)
	{
		hp += 50;
		if hp > 100 hp = 100;
	}
}

if image_index = 0 || image_index = 1
{
	with(obj_player) haskey = true;
}