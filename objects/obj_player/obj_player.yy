{
    "id": "f1e15640-ba9e-46f8-8f45-25a8753fbe1e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "8ce2aadb-e122-4e05-a272-f26b149e2075",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f1e15640-ba9e-46f8-8f45-25a8753fbe1e"
        },
        {
            "id": "47ae820c-a6e6-4db3-b640-93758c1c29b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f1e15640-ba9e-46f8-8f45-25a8753fbe1e"
        },
        {
            "id": "7ea98cf7-34e1-4ff2-9477-ada148d564d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f1e15640-ba9e-46f8-8f45-25a8753fbe1e"
        },
        {
            "id": "3e092a28-2fd1-4440-88ce-80f6e339deb5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e0e498ce-bdbc-471e-b550-1e5089aec076",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f1e15640-ba9e-46f8-8f45-25a8753fbe1e"
        },
        {
            "id": "c39935f1-72f4-4268-8e56-39cb94d43c9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8395e1a5-8226-482b-bf17-c3de8ba54770",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f1e15640-ba9e-46f8-8f45-25a8753fbe1e"
        },
        {
            "id": "bae9ed5e-c58b-4cd9-beca-a253e5f48a26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "f1e15640-ba9e-46f8-8f45-25a8753fbe1e"
        },
        {
            "id": "72104784-6856-4d31-92d9-f9285e41ed58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "f1e15640-ba9e-46f8-8f45-25a8753fbe1e"
        },
        {
            "id": "cb652e89-bd75-4f67-a437-d632baaf7728",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "34188c5a-4d8f-4b25-aa06-190df4870810",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f1e15640-ba9e-46f8-8f45-25a8753fbe1e"
        },
        {
            "id": "dcfe9e5b-7cd9-4909-9dfe-ba043a34a93d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f1e15640-ba9e-46f8-8f45-25a8753fbe1e"
        }
    ],
    "maskSpriteId": "b93e4d0c-a36a-4cfc-b703-9edf832cd601",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "d509f26b-db01-4f64-a05b-b07b6f84cefd",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "4116e94b-6b65-4bd5-9d0c-7fa78cc0f917",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 48,
            "y": 0
        },
        {
            "id": "cf2b030e-a73f-4400-ada6-82376f1675e1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 48,
            "y": 48
        },
        {
            "id": "17239b62-7efe-475e-9410-2fc1fac432d8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 48
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b93e4d0c-a36a-4cfc-b703-9edf832cd601",
    "visible": true
}