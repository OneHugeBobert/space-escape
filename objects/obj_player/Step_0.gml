///@desc Movement, collision, animation, health, lives, time bonus

//Get Player Input

if (hascontrol)
{
	key_left = keyboard_check(vk_left) || keyboard_check(ord("A"));
	key_right = keyboard_check(vk_right) || keyboard_check(ord("D"));
	key_jump = keyboard_check_pressed(vk_up) || keyboard_check_pressed(ord("W"));
}
else
{
	key_right = 0;
	key_left = 0;
	key_jump = 0;
}

//Calculate Movement
var move = key_right - key_left;

hsp = move * walksp;

//Vertical Movement
vsp = vsp + grv;

if (place_meeting(x,y+1,obj_wall))
{
	jumps = maxjumps;
}
if (key_jump) && (jumps > 0)
{
	audio_play_sound(s_jump,10,false);
	jumps = jumps - 1;
	vsp = -7;
}
if(!place_meeting(x,y+1,obj_wall) && jumps==maxjumps) jumps--;
	

//Horizontal Collision
if (place_meeting(x+hsp,y,obj_wall))
{
	while(!place_meeting(x+sign(hsp),y,obj_wall))
	{
		x = x + sign(hsp);
	}
	hsp = 0;
}
x = x + hsp;

//Vertical Collision
if (place_meeting(x,y+vsp,obj_wall))
{
	while(!place_meeting(x,y+sign(vsp),obj_wall))
	{
		y = y + sign(vsp);
	}
	vsp = 0;
}

y = y + vsp;

//Animation
if (!place_meeting(x,y+1,obj_wall))
{
	sprite_index = spr_playerA;
	image_speed = 0;
	if (sign(vsp) > 0) image_index = 1; else image_index = 0;
}
else
{
	if (sprite_index == spr_playerA) audio_play_sound(s_landing,4,false);
	image_speed = 1;
	if (hsp == 0)
	{
		sprite_index = spr_player;
	}
	else
	{
		sprite_index = spr_playerR;
	}
}
if (hsp != 0) image_xscale = sign(hsp);

//If player falls off screen, loses a life and sets them at spawn
if endlevel = false
{
	if obj_player.y > room_height
	{
		score -=500;
		if score < 0 score = 0;
		life -= 1;
		audio_play_sound(s_death,10,false);
		x = obj_spawn.x;
		y = obj_spawn.y;
		hp = 100;
	}
}


//If player's hp = 0, player loses a life and health is set back to full
if hp <=0 && life > 0
{
	score -=500;
	if score < 0 score = 0;
	hp = 100;
	life -=1;
	audio_play_sound(s_death,10,false);
	obj_player.x = obj_spawn.x;
	obj_player.y = obj_spawn.y;
}

//Bonus score
if(alarm[0] == -1)
alarm[0]=room_speed/7.5;