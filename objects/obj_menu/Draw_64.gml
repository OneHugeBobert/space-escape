/// @description Draw Menu

draw_set_font(fMenu);
draw_set_halign(fa_right);
draw_set_valign(fa_bottom);

for (var i = 0; i < menu_items; i ++)
{
	var offset = 3;
	var txt = menu[i];
	if (menu_cursor == i)
	{
		txt = string_insert("> ", txt, 0);
		var col = c_white;
	}
	else
	{
		var col = c_gray;
	}
	var xx = menu_x;
	var yy = menu_y - (menu_itemheight * (i * 1.5));
	draw_set_color(c_black);
	draw_text(xx - offset,yy,txt);
	draw_text(xx + offset,yy,txt);
	draw_text(xx,yy - offset,txt);
	draw_text(xx,yy + offset,txt);
	draw_set_color(col);
	draw_text(xx,yy,txt);
}

//Title Text
draw_set_halign(fa_center);
draw_set_valign(fa_center);
draw_set_font(fTitle);
var text = "Space Escape";
draw_set_color(c_gray);
draw_text(gui_width/2 - offset,200,text);
draw_text(gui_width/2 + offset,200,text);
draw_text(gui_width/2,200 - offset,text);
draw_text(gui_width/2,200 + offset,text);
draw_text_color(gui_width/2,200,text,c_white,c_white,c_gray,c_gray,1);

//Can't see menu options fly off side of screen when pressed
draw_set_color(c_black);
draw_set_halign(fa_left);
draw_rectangle(1025,0,2000,2000,false);