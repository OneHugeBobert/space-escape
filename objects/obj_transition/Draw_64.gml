/// @description Draw black bars

if (mode != TRANS_MODE.OFF)
{
	draw_set_color(c_black);
	draw_rectangle(0,0,w+100,percent*h_half,false);
	draw_rectangle(0,h+100,w+100,h-(percent*h_half),false);
}